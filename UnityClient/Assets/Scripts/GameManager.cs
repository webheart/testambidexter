﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    [SerializeField] private Transform _playerCharacterPivot;
    [SerializeField] private Transform _enemyCharacterPivot;
    [SerializeField] private GameObject _gameOverUi;
    [SerializeField] private GameObject _enemyCharacter;

    void Awake()
    {
        
    }

    void Start()
    {
        Instance = this;
        if (Player.LocalPlayerInstance == null)
        {
            Instantiate(Launcher.Instance.SelectedCharacter, _playerCharacterPivot.position, _playerCharacterPivot.rotation);
        }
        Instantiate(_enemyCharacter, _enemyCharacterPivot.position, _enemyCharacterPivot.rotation);
    }
    
    public void ShowGameoverDialog(bool isPlayerWin)
    {
        GameObject gameOverUi = Instantiate(_gameOverUi, GameObject.Find("UIRoot").GetComponent<Transform>()) as GameObject;
        gameOverUi.SendMessage("SetWinner", isPlayerWin, SendMessageOptions.RequireReceiver);
    }
    public void LeaveRoom()
    {
        SceneManager.LoadScene("MainScene");
    }
    
}
