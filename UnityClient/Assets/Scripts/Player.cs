﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Player : Entity
{
    public static Player LocalPlayerInstance;

    public float Damage = 10f;
    [NonSerialized] public Entity Target;

    void Awake()
    {
        LocalPlayerInstance = this;
    }

    void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            Attack();
        }
    }

    void Attack()
    {
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (!Physics.Raycast(ray, out hit)) return;
        if (!hit.collider.gameObject.GetComponent<Entity>()) return;
        if (hit.collider.gameObject.GetComponent<Entity>() == this) return;
        Target = hit.collider.gameObject.GetComponent<Entity>();
        Debug.Log("attack target:" + Target.gameObject);
        PhotonServer.Instance.SendAttackOperation(Damage);
    }
}