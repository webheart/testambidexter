using System;
using System.Collections.Generic;
using System.Linq;
using ExitGames.Client.Photon;
using TestAmbidexterLib.Common;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PhotonServer : MonoBehaviour, IPhotonPeerListener
{
    private const string CONNECTION_STRING = "localhost:5055";
    private const string APP_NAME = "MyServer";

    private static PhotonServer _instance;

    public static PhotonServer Instance
    {
        get { return _instance; }
    }

    private PhotonPeer PhotonPeer { get; set; }

    void Awake()
    {
        if (Instance != null)
        {
            DestroyObject(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        Application.runInBackground = true;

        _instance = this;
    }

    void Start()
    {
        PhotonPeer = new PhotonPeer(this, ConnectionProtocol.Udp);
        Connect();
    }

    void Update()
    {
        if (PhotonPeer != null)
            PhotonPeer.Service();
    }

    void OnApplicationQuit()
    {
        Disconnect();
    }

    public static string GetUniqueID()
    {
        if (!PlayerPrefs.HasKey("GUID"))
        {
            var myGuid = Guid.NewGuid().ToString();
            PlayerPrefs.SetString("GUID", myGuid);
        }
        Debug.Log(PlayerPrefs.GetString("GUID"));
        return PlayerPrefs.GetString("GUID");
    }

    private void Connect()
    {
        if (PhotonPeer != null)
            PhotonPeer.Connect(CONNECTION_STRING, APP_NAME);
    }

    private void Disconnect()
    {
        if (PhotonPeer != null)
            PhotonPeer.Disconnect();
    }

    public void DebugReturn(DebugLevel level, string message)
    {
        Debug.Log("DebugReturn level:" + level.ToString());
    }

    public void OnOperationResponse(OperationResponse operationResponse)
    {
        switch (operationResponse.OperationCode)
        {
            case (byte) OperationCode.Attack:
                if (operationResponse.Parameters.ContainsKey((byte) ParameterCode.Damage) &&
                    Player.LocalPlayerInstance.Target != null)
                {
                    Player.LocalPlayerInstance.Target.GetDamage(
                        (float) operationResponse.Parameters[(byte) ParameterCode.Damage]);
                    if (Player.LocalPlayerInstance.Target.CurrentHealth <= 0)
                    {
                        SendGameoverOperation(true, Launcher.Instance.CurrentCharacterIndex);
                        GameManager.Instance.ShowGameoverDialog(true);
                    }
                }
                break;
            case (byte)OperationCode.GetStats:
                if (operationResponse.Parameters.ContainsKey((byte)ParameterCode.CharacterIndex))
                {
                    Debug.Log("event.updateStats");
                    var stats = (int[])operationResponse.Parameters[(byte)ParameterCode.CharacterIndex];
                    Launcher.Instance.UpdateStats(stats);
                }
                break;
            default:
                Debug.Log("Unknown OperationResponse:" + operationResponse.OperationCode);
                break;
        }
    }

    public void OnEvent(EventData eventData)
    {
        switch (eventData.Code)
        {
            case (byte) EventCode.UpdateStats:
                
                SendUpdateStatOperation(Launcher.Instance.CurrentCharacterIndex);
                break;
            default:
                Debug.Log("Unknown Event:" + eventData.Code);
                break;
        }
    }

    public void OnStatusChanged(StatusCode statusCode)
    {
        switch (statusCode)
        {
            case StatusCode.Connect:
                Debug.Log("Connected to server!");
                SendGuidOperation(GetUniqueID());
                SceneManager.LoadScene("MainScene");
                break;
            case StatusCode.Disconnect:
                Debug.Log("Disconnected from server!");
                break;
            case StatusCode.TimeoutDisconnect:
                Debug.Log("TimeoutDisconnected from server!");
                break;
            case StatusCode.DisconnectByServer:
                Debug.Log("DisconnectedByServer from server!");
                break;
            case StatusCode.DisconnectByServerUserLimit:
                Debug.Log("DisconnectedByLimit from server!");
                break;
            case StatusCode.DisconnectByServerLogic:
                Debug.Log("DisconnectedByLogic from server!");
                break;
            case StatusCode.EncryptionEstablished:
                break;
            case StatusCode.EncryptionFailedToEstablish:
                break;
            default:
                Debug.Log("Unknown status:" + statusCode.ToString());
                break;
        }
    }

    #region Up-level API
    public void SendCharactersListOperation(int charsCount)
    {
        PhotonPeer.OpCustom((byte)OperationCode.UpdateCharactersCount, new Dictionary<byte, object>
        {
            {(byte) ParameterCode.CharactersCount, charsCount},
        }, true);
    }
    public void SendAttackOperation(float damage)
    {
        PhotonPeer.OpCustom((byte) OperationCode.Attack, new Dictionary<byte, object>
        {
            {(byte) ParameterCode.Damage, damage},
        }, true);
    }
    public void SendGuidOperation(string guid)
    {
        PhotonPeer.OpCustom((byte)OperationCode.GetGUID, new Dictionary<byte, object>
        {
            {(byte) ParameterCode.GUID, guid},
        }, true);
    }
    public void SendUpdateStatOperation(int characterIndex)
    {
        PhotonPeer.OpCustom((byte)OperationCode.GetStats, new Dictionary<byte, object>
        {
            {(byte) ParameterCode.CharacterIndex, characterIndex},
        }, true);
    }
    public void SendGameoverOperation(bool isPlayerWin, int characterIndex)
    {
        PhotonPeer.OpCustom((byte)OperationCode.Gameover, new Dictionary<byte, object>
        {
            {(byte) ParameterCode.IsPlayerWin, isPlayerWin},
            {(byte) ParameterCode.CharacterIndex, characterIndex},
        }, true);
    }
    #endregion
}