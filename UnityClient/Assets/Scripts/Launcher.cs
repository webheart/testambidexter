﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Launcher : MonoBehaviour
{
    public static Launcher Instance;
    public GameObject SelectedCharacter;
    public int CurrentCharacterIndex = 0;
    [SerializeField] private GameObject _controlPanel;
    [SerializeField] private GameObject _progressLabel;
    [SerializeField] private List<GameObject> _playerPrefabsList;
    [SerializeField] private Transform _characterPivot;
    [SerializeField] private Text _characterName;
    [SerializeField] private Text _winsText;
    [SerializeField] private Text _defeatsText;

    private readonly List<string> _scenes = new List<string>{"BattleScene1", "BattleScene2", "BattleScene3"};
    private int _currentSceneIndex;
    private int _wins;
    private int _defeats;

    void Awake()
    {
        Instance = this;
    }
    
    void Start()
    {
        PhotonServer.Instance.SendCharactersListOperation(_playerPrefabsList.Count);
        ChangeCharacter(_playerPrefabsList[CurrentCharacterIndex]);
    }

    void Update()
    {
        if (_winsText.text != _wins.ToString() || _defeatsText.text != _defeats.ToString())
        {
            _winsText.text = _wins.ToString();
            _defeatsText.text = _defeats.ToString();
        }
    }
    void ChangeCharacter(GameObject playerObject)
    {
        for (int i = 0; i < _characterPivot.childCount; i++)
        {
            Destroy(_characterPivot.GetChild(i).gameObject);
        }

        Instantiate(playerObject, _characterPivot.position, _characterPivot.rotation, _characterPivot);
        _characterName.text = playerObject.name;
        SelectedCharacter = playerObject;
        PhotonServer.Instance.SendUpdateStatOperation(CurrentCharacterIndex);
    }
    void LoadBattleScene()
    {
        SceneManager.LoadScene(_scenes[_currentSceneIndex]);
    }

    public void UpdateStats(int[] stats)
    {
        _wins = stats[0];
        _defeats = stats[1];
        Debug.Log("Update stats: " + stats[0] + stats[1]);
    }
    #region ButtonHandlers
    
    public void OnClickNextCharacter()
    {
        CurrentCharacterIndex++;
        if (CurrentCharacterIndex >= _playerPrefabsList.Count) CurrentCharacterIndex = 0;

        ChangeCharacter(_playerPrefabsList[CurrentCharacterIndex]);
    }
    public void OnClickPreviousCharacter()
    {
        CurrentCharacterIndex--;
        if (CurrentCharacterIndex < 0) CurrentCharacterIndex = _playerPrefabsList.Count - 1;

        ChangeCharacter(_playerPrefabsList[CurrentCharacterIndex]);
    }

    public void OnClickPlayScene1()
    {
        _currentSceneIndex = 0;
        LoadBattleScene();
    }
    public void OnClickPlayScene2()
    {
        _currentSceneIndex = 1;
        LoadBattleScene();
    }
    public void OnClickPlayScene3()
    {
        _currentSceneIndex = 2;
        LoadBattleScene();
    }

    #endregion
}
