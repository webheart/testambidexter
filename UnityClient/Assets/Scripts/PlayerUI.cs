﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

public class PlayerUI : MonoBehaviour
{

    #region Public Properties

    [Tooltip("Pixel offset from the player target")]
    [SerializeField] private Vector3 _screenOffset = new Vector3(0f, 30f, 0f);
    
    [SerializeField] private Text _currentPlayerHealthText;
    [SerializeField] private Text _maxPlayerHealthText;
    [SerializeField] private Slider _playerHealthSlider;
    
    #endregion

    #region Private Properties

    Entity _target;

    float _characterHeight = 2f;

    Transform _targetTransform;

    Renderer _targetRenderer;

    Vector3 _targetPosition;

    #endregion

    #region MonoBehaviour Messages
    
    void Awake()
    {
        this.GetComponent<Transform>().SetParent(GameObject.Find("UIRoot").GetComponent<Transform>());
    }
    void Update()
    {
        // Destroy itself if the target is null, It's a fail safe when Photon is destroying Instances of a Player over the network
        if (_target == null)
        {
            Destroy(this.gameObject);
            return;
        }
        
        _playerHealthSlider.value = _target.CurrentHealth;
        _currentPlayerHealthText.text = _target.CurrentHealth.ToString();
    }
    void LateUpdate()
    {

        // Do not show the UI if we are not visible to the camera, thus avoid potential bugs with seeing the UI, but not the player itself.
        if (_targetRenderer != null)
        {
            this.gameObject.SetActive(_targetRenderer.isVisible);
        }

        // #Critical
        // Follow the Target GameObject on screen.
        if (_targetTransform != null)
        {
            _targetPosition = _targetTransform.position;
            _targetPosition.y += _characterHeight;

            this.transform.position = Camera.main.WorldToScreenPoint(_targetPosition) + _screenOffset;
        }
    }
    #endregion

    #region Public Methods
    /// <summary>
    /// Assigns a Player Target to Follow and represent.
    /// </summary>
    /// <param name="target">Target.</param>
    public void SetTarget(Entity target)
    {

        if (target == null)
        {
            Debug.LogError("<Color=Red><b>Missing</b></Color> PlayMakerManager target for PlayerUI.SetTarget.", this);
            return;
        }

        // Cache references for efficiency because we are going to reuse them.
        _target = target;
        _targetTransform = _target.GetComponent<Transform>();
        _targetRenderer = _target.GetComponent<Renderer>();
        
        _playerHealthSlider.maxValue = _target.MaxHealth;
        _maxPlayerHealthText.text = _target.MaxHealth.ToString();
        
    }

    #endregion

}