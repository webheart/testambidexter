﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

public class GameoverUI : MonoBehaviour
{
    [SerializeField] private Text _gameoverText;
    
    public void SetWinner(bool isPlayerWin)
    {
        _gameoverText.text = isPlayerWin ? "Victory!" : "Defeat!";
    }

    public void OnClickExit()
    {
        GameManager.Instance.LeaveRoom();
    }
}