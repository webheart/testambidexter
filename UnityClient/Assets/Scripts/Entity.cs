﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour
{
    public float CurrentHealth = 100f;
    public float MaxHealth = 100f;
    
    [SerializeField] private GameObject _playerUiPrefab;
    
    void Start()
    {
        if (_playerUiPrefab != null)
        {
            GameObject uiGo = Instantiate(_playerUiPrefab) as GameObject;
            uiGo.SendMessage("SetTarget", this, SendMessageOptions.RequireReceiver);
        }
        else
        {
            Debug.LogWarning("<Color=Red><a>Missing</a></Color> PlayerUiPrefab reference on player Prefab.", this);
        }
    }
    
    public void GetDamage(float receivedDamage)
    {
        CurrentHealth -= receivedDamage;
    }
}
