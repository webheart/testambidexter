﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestAmbidexterLib.Common
{
    public enum ParameterCode : byte
    {
        Damage, GUID, CharacterIndex, IsPlayerWin, CharactersCount
    }
}