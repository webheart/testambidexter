﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestAmbidexterLib.Common
{
    public enum ErrorCode : byte
    {
        Ok, InvalidParameters, RequestNotImplemented
    }
}
