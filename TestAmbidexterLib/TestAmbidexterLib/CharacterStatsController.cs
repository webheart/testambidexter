﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TestAmbidexterLib.Annotations;

namespace TestAmbidexterLib
{
    public class CharacterStatsController
    {
        /// <summary>
        /// Dictionary(GUID, SharacterStats)
        /// </summary>
        public Dictionary<string, CharacterStats[]> Characters = new Dictionary<string, CharacterStats[]>();
    }

    public class CharacterStats
    {
        public event EventHandler OnStatsChanged;
        private int _wins;
        private int _defeats;

        public int Wins
        {
            get { return _wins; }
            set
            {
                _wins = value;
                OnStatsChanged?.Invoke(this, null);
            }
        }

        public int Defeats
        {
            get { return _defeats; }
            set
            {
                _defeats = value;
                OnStatsChanged?.Invoke(this, null);
            }
        }
    }
}