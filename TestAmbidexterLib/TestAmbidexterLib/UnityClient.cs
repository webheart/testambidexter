﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using ExitGames.Logging;
using Photon.SocketServer;
using PhotonHostRuntimeInterfaces;
using TestAmbidexterLib.Common;

namespace TestAmbidexterLib
{
    public class UnityClient : ClientPeer
    {
        public string GUID;
        private readonly ILogger Log = LogManager.GetCurrentClassLogger();
        private CharacterStatsController _characterStats;

        public UnityClient(InitRequest initRequest, IPhotonPeer unmanagedPeer, CharacterStatsController characterStats)
            : base(initRequest)
        {
            Log.Debug("Client ip:" + unmanagedPeer.GetRemoteIP());
            _characterStats = characterStats;
        }

        protected override void OnOperationRequest(OperationRequest operationRequest, SendParameters sendParameters)
        {
            switch (operationRequest.OperationCode)
            {
                case (byte) OperationCode.Attack:
                {
                    if (operationRequest.Parameters.ContainsKey((byte) ParameterCode.Damage))
                    {
                        OperationResponse response = new OperationResponse(operationRequest.OperationCode);
                        response.Parameters = new Dictionary<byte, object>
                        {
                            {(byte) ParameterCode.Damage, operationRequest.Parameters[(byte) ParameterCode.Damage]}
                        };
                        SendOperationResponse(response, sendParameters);
                    }
                }
                    break;
                case (byte) OperationCode.GetGUID:
                {
                    if (operationRequest.Parameters.ContainsKey((byte) ParameterCode.GUID))
                    {
                        GUID = (string) operationRequest.Parameters[(byte) ParameterCode.GUID];
                        Log.Debug(GUID);
                    }
                }
                    break;
                case (byte) OperationCode.GetStats:
                {
                    if (operationRequest.Parameters.ContainsKey((byte) ParameterCode.CharacterIndex))
                    {
                        var characterIndex = (int) operationRequest.Parameters[(byte) ParameterCode.CharacterIndex];

                        CharacterStats charStats = _characterStats.Characters[GUID][characterIndex];
                        var wins = charStats.Wins;
                        var defeats = charStats.Defeats;

                        var response = new OperationResponse(operationRequest.OperationCode)
                        {
                            Parameters = new Dictionary<byte, object>
                            {
                                {(byte) ParameterCode.CharacterIndex, new[] {wins, defeats}}
                            }
                        };
                        SendOperationResponse(response, sendParameters);
                    }
                }
                    break;
                case (byte) OperationCode.Gameover:
                {
                    if (operationRequest.Parameters.ContainsKey((byte) ParameterCode.CharacterIndex) &&
                        operationRequest.Parameters.ContainsKey((byte) ParameterCode.IsPlayerWin))
                    {
                        var isWin = (bool) operationRequest.Parameters[(byte) ParameterCode.IsPlayerWin];
                        //var isDefeat = !(bool) operationRequest.Parameters[(byte) ParameterCode.IsPlayerWin];
                        //UpdateStatsHandler(operationRequest, sendParameters, isWin, isDefeat);
                        var characterIndex = (int) operationRequest.Parameters[(byte) ParameterCode.CharacterIndex];

                        CharacterStats charStats = _characterStats.Characters[GUID][characterIndex];
                        if (isWin)
                        {
                            charStats.Wins++;
                        }
                        else
                        {
                            charStats.Defeats++;
                        }
                    }
                }
                    break;
                case (byte) OperationCode.UpdateCharactersCount:
                {
                    if (operationRequest.Parameters.ContainsKey((byte) ParameterCode.CharactersCount))
                    {
                        var count = (int) operationRequest.Parameters[(byte) ParameterCode.CharactersCount];
                        Log.Debug("Characters Count: " + count);
                        if (!_characterStats.Characters.ContainsKey(GUID))
                        {
                            var stats = new CharacterStats[count];
                            for (int i = 0; i < stats.Length; i++)
                            {
                                stats[i] = new CharacterStats();
                                stats[i].OnStatsChanged += UpdateStatsHandler;
                            }
                            _characterStats.Characters.Add(GUID, stats);
                        }
                    }
                }
                    break;
                default:
                    Log.Debug("Wrong OperationRequest");
                    break;
            }
        }

        void UpdateStatsHandler(object sender, EventArgs eventArgs)
        {
            var eventData = new EventData();
            eventData.Code = (byte)EventCode.UpdateStats;
            SendEvent(eventData, new SendParameters());
        }

        protected override void OnDisconnect(DisconnectReason reasonCode, string reasonDetail)
        {
            Log.Debug("Disconnected!");
        }
    }
}