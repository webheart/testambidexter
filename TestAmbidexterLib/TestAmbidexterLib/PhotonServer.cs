﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ExitGames.Logging;
using ExitGames.Logging.Log4Net;
using log4net.Config;
using Photon.SocketServer;

namespace TestAmbidexterLib
{
    public class PhotonServer : ApplicationBase
    {
        private readonly ILogger Log = LogManager.GetCurrentClassLogger();
        private readonly CharacterStatsController _characterStats = new CharacterStatsController();
        
        protected override PeerBase CreatePeer(InitRequest initRequest)
        {
            Log.Debug("connect new client...");
            var client = new UnityClient(initRequest, initRequest.PhotonPeer, _characterStats);
            return client;
        }

        async void DisplayResultAsync()
        {
            await ResetStatsAsync(_characterStats);
        }

        Task ResetStatsAsync(CharacterStatsController characterStats)
        {
            return Task.Run(() =>
            {
                while (true)
                {
                    var sleepTime = 60000;
                    Thread.Sleep(sleepTime);
                    foreach (var character in characterStats.Characters)
                    {
                        foreach (CharacterStats stats in character.Value)
                        {
                            stats.Wins = 0;
                            stats.Defeats = 0;
                        }
                    }
                }
            });
        }

        protected override void Setup()
        {
            var file = new FileInfo(Path.Combine(BinaryPath, "log4net.config"));
            if (file.Exists)
            {
                LogManager.SetLoggerFactory(Log4NetLoggerFactory.Instance);
                XmlConfigurator.ConfigureAndWatch(file);
            }
            DisplayResultAsync();
            Log.Debug("Server is setup");
        }

        protected override void TearDown()
        {
            Log.Debug("Server is closed");
        }
    }
}